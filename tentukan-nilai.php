<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih PHP-3</title>
</head>
<body>

<h3>Soal Nomer 1</h3>

<?php

function tentukan_nilai($number)
{
    if($number >= 85 && $number <= 100){
        echo "Nilai : $number, Sangat baik";
    } else if($number >= 70 && $number < 85){
        echo "Nilai : $number, Baik";
    } else if($number >= 60 && $number < 70){
        echo "Nilai : $number, Cukup";
    } else {
        echo "Nilai : $number, Kurang";
    };
};

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
echo "<br>";
?>
    
</body>
</html>