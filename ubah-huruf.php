<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih PHP-3</title>
</head>
<body>

    <h3>Soal Nomer 2</h3>
    
    <?php
    function ubah_huruf($string){
        $abjad = "abcdefghijklmnopqrstuvwxyz";
        $output = "";
        for ($a = 0; $a < strlen($string); $a++){
            $position = strpos($abjad, $string[$a]);
            $output .= substr($abjad, $position + 1,1);
        };
        echo "kata : $string, output : ";
        return $output . "<br>";
    };

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>

</body>
</html>